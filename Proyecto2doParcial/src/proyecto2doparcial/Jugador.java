/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2doparcial;

import javafx.scene.layout.Pane;

/**
 *
 * @author Ivan Cedeno
 * @author Steffy Freire
 * @author Kleber Avelino
 */
public class Jugador 
{
    private String nombre;
    private int score;
    private int vidas;
    private boolean gameover;
    private PirataPrincipal pirata;

    public Jugador(String nombre, Pane panel,int i) {
        this.nombre = nombre;
        this.score = Constante.SCORE_INICIAL;
        this.vidas = Constante.VIDAS_INICIAL;
        pirata= new PirataPrincipal(panel,i);
        this.gameover = false;
    }
    public Jugador(String nombre, int score)
    {
        this.nombre = nombre;
        this.score = score;
    }

    public boolean isGameover() {
        return gameover;
    }

    public void setGameover(boolean gameover) {
        this.gameover = gameover;
    }

    public PirataPrincipal getPirata() {
        return pirata;
    }

    public void setPirata(PirataPrincipal nave) {
        this.pirata = nave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getVidas() {
        return vidas;
    }

    public void setVidas(int vidas) {
        this.vidas = vidas;
    }

    
    public int compareTo(Jugador o) 
    {
        if (this.score > o.score)
            return 1;
        else if(this.score < o.score)
            return -1;
        else
            return 0;
    }

    
}
