/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2doparcial;

/**
 *
 * * @author Steffy Freire
 * @author Kleber Avelino
 * @author Ivan Cedeño
 */
public class Constante {
    
    public static final double ALTO = 480;
    public static final double ANCHO = 640;
    
    public static final double ALTO_BALA_PIRATA = 10;
    public static final double ANCHO_BALA_PIRATA = 10;
    
    public static final double ALTO_PROYECTIL = 5;
    public static final double ANCHO_PROYECTIL = 5;
    
    public static final double ALTO_BOMBA = 30;
    public static final double ANCHO_BOMBA = 30;
    
    public static final String FONDO_INICIO = "src\\imagenes\\FondoInicial.jpg";
    public static final String FONDO = "src\\imagenes\\FondoJuego.jpg";
    public static final String FONDO_MENU = "src\\imagenes\\FondoMenu.jpg";
    public static final String FONDO_WIN = "src\\imagenes\\FondoYouWin.jpg";
    public static final String FONDO_OVER = "src\\imagenes\\FondoGameOver.jpg";
    public static final String ELIGE_CAP = "src\\imagenes\\ElijeCapitan.jpg";
    public static final String PIRATA_1 = "src\\imagenes\\pirata1.jpg";
    public static final String PIRATA_Z = "src\\imagenes\\pirataZombi.jpg";
    public static final String BOTON_JUEGO = "src\\imagenes\\bNewGame.png";
    public static final String BOTON_HISTORIAL = "src\\imagenes\\hitorial.png";
    public static final String BOTON_CREDITOS = "src\\imagenes\\bCreditos.png";
    public static final String BOTON_INSTRUCCIONES = "src\\imagenes\\intruction.png";
    public static final String MENU_INSTRUCCIONES = "src\\imagenes\\fondointruc.jpg";
    
    public static final String BOTON_SALIR = "src\\imagenes\\bSalir.png";
     public static final String BOT_JACK = "src\\imagenes\\botJack.jpg";
    public static final String BOT_BARBOSA= "src\\imagenes\\botBarbosa.jpg";
    public static final String BOT_DAVID_JONES = "src\\imagenes\\botDavidJones.jpg";
    public static final String BOMBA = "src\\imagenes\\bomb.png";
    
    public static final double ALTO_PIRATA_P = 40;
    public static final double ANCHO_PIRATA_P = 40;
    public static final double PIRATA_X = 200;
    public static final double PIRATA_Y = 380;
    
    public static final double ALTO_PIRATA_E =25;
    public static final double ANCHO_PIRATA_E = 40;
    
    public static final int SCORE_INICIAL =0;
    public static final int VIDAS_INICIAL =1;
    
    public static final double RECORRIDO_PIRATA = 10;
}
