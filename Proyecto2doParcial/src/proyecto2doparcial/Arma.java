/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2doparcial;

/**
 * @author Ivan Cedeno
 * @author Steffy Freire
 * @author Kleber Avelino
Creamos esta clase de la cual heredaran las armas bala y bomba
*/
public abstract class Arma {
    private double alto;
    private double ancho;
    private double centroX;
    private double centroY;
    private boolean visible;
/*
    el constructor de Arma recibe el tamaño de alto y ancho, además de 
    las coordenadas de X y Y y se inicializa la propiedad de visible
    */
    public Arma(double alto, double ancho, double centroX, double centroY) {
        this.alto = alto;
        this.ancho = ancho;
        this.centroX = centroX;
        this.centroY = centroY;
        this.visible = true;
    }

    /*
    retorna el valor logico de visibilidad del arma
    */
    public boolean isVisible() {
        return visible;
    }

    /*
    permite dar un valos a la propiedad visible
    */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }
/*
    retorna el valor del alto del arma
    */
    public double getAlto() {
        return alto;
    }
/*
    permite dar un valor a la propiedad alto   
    */
    public void setAlto(double alto) {
        this.alto = alto;
    }
/*
    retorna el valor de la propiedad ancho
    */
    public double getAncho() {
        return ancho;
    }
/*
    permite dar un valor a la propiedad de ancho
    */
    public void setAncho(double ancho) {
        this.ancho = ancho;
    }
/*
    retorna el valor de la propiedad centroX
    */
    public double getCentroX() {
        return centroX;
    }
/*
    permite dar un valor a la propiedad centroX
    */
    public void setCentroX(double centroX) {
        this.centroX = centroX;
    }    
/*
    retorna el valor de la propiedad centroY
    */
    public double getCentroY() {
        return centroY;
    }
/*
    permite dar el valor de la propiedad centroY
    */
    public void setCentroY(double centroY) {
        this.centroY = centroY;
    }
    /*
    metodo que debe ser implementado para mover el arma
    */
    public abstract double mover(int i);
    /*
    metodo que debe ser implementado que destruye el arma
    */
    public abstract void destruir();
}
