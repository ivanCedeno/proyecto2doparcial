/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2doparcial;

import java.io.File;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;

/**
 *
 * @author Ivan Cedeno
 *  @author Steffy Freire
 * @author Kleber Avelino
 */
public  class  Sonido 
{    
    private static String sonido;
    private static Media media;
    private static MediaPlayer mplayer;
    private static MediaView mv;
    /*
    sonido producido cuando se presiona la tecla space
    */
    public static void disparoBala()
    {   
        sonido = "src\\sonidos\\shoot.mp3";
        play();        
    }
    /*
    Sonido producido cuando se presiona la tecla M
    */
    public static void disparoBomba()
    {
        sonido = "src\\sonidos\\bomb.mp3";
        play();
    }
  
    public static void sonidoFondo()
    {
        sonido = "src\\sonidos\\fondo.mp3";
        play();
        
    }   
    public static void play()
    {
        media = new Media(new File(sonido).toURI().toString());
        mplayer = new MediaPlayer(media);
        mplayer.setAutoPlay(true);
        mv = new MediaView(mplayer);
    }
}
