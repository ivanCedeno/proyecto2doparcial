/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2doparcial;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.*;
import javafx.util.Duration;

/**
 *
 * @author Ivan Cedeño
 *@author Steffy Freire
 * @author Kleber Avelino
 */
public class ArmaBomba extends Arma 
    {
     private Ellipse bomb;
    private Pane panel;

    public Ellipse getBomb() {
        return bomb;
    }

    public void setBomb(Ellipse bomb) {
        this.bomb = bomb;
    }
/*
    El constructor recibe las coordenadas X y Y además de el panel en donde se ubicara el objeto
    */
    public ArmaBomba(double centroX, double centroY, Pane panel) {
        super(Constante.ALTO_BOMBA, Constante.ANCHO_BOMBA, centroX, centroY);
        bomb = new Ellipse();
       
        this.setupbomb();
        this.panel = panel;
        this.panel.getChildren().add(bomb);
    }
    /*
    Se inicializa las caracteristicas del objeto
    */
    
    public void setupbomb()
    {
        Image boom= new Image("file:src\\imagenes\\explosion.png");
        this.bomb.setVisible(this.isVisible());
        this.bomb.setFill(new ImagePattern(boom));
         this.bomb.setRadiusX(this.getAncho());
        this.bomb.setRadiusY(this.getAlto());
        this.bomb.setCenterX(this.getCentroX()+230);
        this.bomb.setCenterY(this.getCentroY()+30);
       
    }
/*
    se sobreescribe el metodo mover de la clase Arma
    */
    @Override
    public double mover(int i) 
    {
        KeyValue xCoord = new KeyValue (bomb.centerXProperty(), bomb.getCenterX()+i);
        KeyFrame f = new KeyFrame (Duration.seconds(0.7), xCoord);
        Timeline animation = new Timeline(f);
        animation.play();
        this.setCentroX(bomb.getCenterX()+i);     
        return this.getCentroX()-1;       
    }
/*
    se sobreescribe el metodo destruir de la clase arma
    */
    @Override
    public void destruir() {
    if (!this.isVisible())
        {
            for (int x = 0; x< this.bomb.getRadiusX()+10;x++)
                {
                    KeyValue radioy = new KeyValue (this.bomb.radiusYProperty(), this.bomb.getRadiusY()-x);
                    KeyValue radiox = new KeyValue (this.bomb.radiusXProperty(), this.bomb.getRadiusX()-x);
                    KeyFrame f1 = new KeyFrame (Duration.seconds(3.5), radioy);                                     
                    KeyFrame f2 = new KeyFrame (Duration.seconds(3.5), radiox);                                     
                    Timeline anrioY = new Timeline(f1);
                    Timeline anrioX = new Timeline(f2);
                    anrioY.play();
                    anrioX.play();
                }
        }
}
    }
