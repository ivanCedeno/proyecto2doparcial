/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2doparcial;

import javafx.scene.image.ImageView;

/**
 *
 * @author Ivan Cedeno
 * @author Steffy Freire
 * @author Kleber Avelino
 */
public abstract class Piratas 
{
    private double alto;
    private double ancho;
    private double centroX;
    private double centroY;
    private boolean visible;

  
    
    public abstract void  moverY(double cantidad);
    public abstract void setupPirata();
    public abstract boolean isMover(double cantidad);

    public Piratas(double alto, double ancho, double centroX, double centroY) {
        this.alto = alto;
        this.ancho = ancho;
        this.centroX = centroX;
        this.centroY = centroY;
        this.visible= true;
      
        
    }


  
    

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
    

    public double getAlto() {
        return alto;
    }

    public void setAlto(double alto) {
        this.alto = alto;
    }

    public double getAncho() {
        return ancho;
    }

    public void setAncho(double ancho) {
        this.ancho = ancho;
    }

    public double getCentroX() {
        return centroX;
    }

    public void setCentroX(double centroX) {
        this.centroX = centroX;
      
    }

    public double getCentroY() {
       
        return centroY;
    }

    public void setCentroY(double centroY) {
       
        this.centroY = centroY;
      
    }
    
    
}
