/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2doparcial;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.Scanner;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Ivan Cedeno
 * @author Steffy Freire
 * @author Kleber Avelino
 */
public class Archivo {
    private String nombre;
    private String tiempo;
    private String monedas;
    private String fecha;
    private String nivel;
    private String gano;

    public Archivo() {
    }

    public Archivo(String nombre, String tiempo, String monedas, String fecha, String nivel, String gano) {
        this.nombre = nombre;
        this.tiempo = tiempo;
        this.monedas = monedas;
        this.fecha = fecha;
        this.nivel = nivel;
        this.gano = gano;
    }

    

    public ImageView obtenerImagen(String ruta,double alto, double ancho)
    {
        Image fondo;
        try 
        {
            InputStream img1 = Files.newInputStream(Paths.get(ruta));
            fondo = new Image(img1);
            ImageView fondoView = new ImageView(fondo);
            fondoView.setFitHeight(alto);
            fondoView.setFitWidth(ancho);
            return fondoView;
        }
        catch(IOException e)
        {
            System.out.println("No se encuentra la imagen");
        }
        return null;
    }
     @Override
    public String toString() {
        return nombre+","+tiempo+","+monedas+","+fecha+","+nivel+","+gano;
    }  
         public static LinkedList<Archivo> cargarArchivos() throws FileNotFoundException, IOException{
        LinkedList<Archivo> listaArchivo=new LinkedList<>();
        String linea;
        
        try{
            FileReader f=new FileReader("archivo.txt");
            BufferedReader b=new BufferedReader(f);
            Scanner sc=new Scanner(b);
            while(sc.hasNext()){
                linea=sc.nextLine();
                String[] nuevo=linea.split("\\,");
                Archivo archivos=new Archivo(nuevo[0],nuevo[1],nuevo[2],nuevo[3],nuevo[4],nuevo[5]);
                listaArchivo.add(archivos);
            }
        }
        catch(Exception e){}
        return listaArchivo;
    }
          public void IngresarNuevosDatos(LinkedList<Archivo> archivos,String nombre,String tiempo,String monedas,String fecha,String nivel,String gano) throws IOException{
            
     Archivo ar= new Archivo(nombre,tiempo,monedas,fecha,nivel,gano);
     archivos.add(ar);
     FileWriter arch=new FileWriter("archivo.txt",true);
     PrintWriter pw=new PrintWriter(arch);
        try{
            pw.println(ar.toString());
        }
        catch(Exception e){
        }
        finally{
            try{
                if(null!=arch)
                    arch.close();
            }
            catch(Exception f){
            }
        }
 }
}