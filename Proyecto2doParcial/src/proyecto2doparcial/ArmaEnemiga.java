/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2doparcial;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.util.Duration;

/**
 *
 * @author Ivan Cedeno
 *  @author Steffy Freire
 * @author Kleber Avelino
 * Esta clase no es usada pero la cree para mejorar el juego en un futuro
 * aun no funciona correctamente.
 */
public class ArmaEnemiga extends Arma implements Runnable
{
 /*
    el constructor recibe las coordenadas de X y Y y el panel en donde se ubicará el objeto
    */   
    private PirataPrincipal pp;
    private Ellipse base;
    private boolean impacto;
    private boolean impactoPirata;
    private boolean noImpacto;

    public boolean isImpactoPirata() {
        return impactoPirata;
    }

    public void setImpactoPirata(boolean impactoNave) {
        this.impactoPirata = impactoNave;
    }

    public boolean isNoImpacto() {
        return noImpacto;
    }

    public void setNoImpacto(boolean noImpacto) {
        this.noImpacto = noImpacto;
    }
    
    public ArmaEnemiga(double centroX, double centroY, Pane panel, PirataPrincipal pp) {
        super(Constante.ALTO_PROYECTIL, Constante.ANCHO_PROYECTIL, centroX, centroY);
        impacto = false;
        this.impactoPirata=false;
        this.noImpacto=false;
        this.base = new Ellipse();
        this.setup();
        this.pp = pp;
    }
    public void setup()
    {
        this.base.setFill(Color.CHOCOLATE);
        this.base.setCenterX(this.getCentroX());
        this.base.setCenterY(this.getCentroY());
        this.base.setRadiusX(5);
        this.base.setRadiusY(5);
    }

    @Override
    public double mover(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void destruir() 
    {
        if (!this.isVisible())
        {
            for (int x = 0; x< this.base.getRadiusX()+4;x++)
                {
                    KeyValue radioy = new KeyValue (this.base.radiusYProperty(), this.base.getRadiusY()-x);
                    KeyValue radiox = new KeyValue (this.base.radiusXProperty(), this.base.getRadiusX()-x);
                    KeyFrame f1 = new KeyFrame (Duration.seconds(0.7), radioy);                                     
                    KeyFrame f2 = new KeyFrame (Duration.seconds(0.7), radiox);                                     
                    Timeline anrioY = new Timeline(f1);
                    Timeline anrioX = new Timeline(f2);
                    anrioY.play();
                    anrioX.play();
                }
        }
    }

    @Override
    public void run() {
        while(!impacto)
        {
            try {
                this.setCentroY(this.getCentroY()+1);
                if (this.getCentroY() == pp.getCentroY() && this.getCentroX() == pp.getCentroY())
                {
                    impacto = true;
                    this.impactoPirata = true;                    
                    this.destruir();
                }
                else if (this.getCentroY() == Constante.ALTO -5)
                {
                    this.noImpacto = true;
                    impacto = true;
                    this.destruir();
                }
                Thread.sleep(20);
            } catch (InterruptedException ex) {
                Logger.getLogger(ArmaEnemiga.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
}

