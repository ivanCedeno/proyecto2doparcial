/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2doparcial;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Timeline;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;
import javafx.util.Duration;

/**
 *
 * @author Ivan Cedeno
 *  @author Steffy Freire
 * @author Kleber Avelino
 */
public class Juego
{
    private BorderPane root;
    private Label puntaje, vidas,balas,bombas;
    private boolean pause;
    private Stage stage;
    private Pane jugar;
    private Pane bomb;
    private Nivel level;
    private Stage levelCompleto;
    private int nivel,tmp;
    private int score;
    private int vida;
    private long velocidad;
    private double orgSceneX, orgSceneY,orgTranslateX,orgTranslateY,nX,nY;
    private int piratip;
    private DateFormat sdf;
    private Date date;
    private String fechaHora,fecha,horas,hora,minutos,segundos;
    private double cx,cy;
     
    
        public Juego(String nombre, int score,Stage stage,Stage salir)
    {             
        this.cx = 230;
        this.cy= 35;
        this.vida = 1;
        this.score = score;
        this.stage = stage;
        this.pause = false;
        this.root = new BorderPane();
        this.sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        this.date = new Date();
        this.fechaHora=sdf.format(date);
        String delimiter=" ";
        String[]xv=fechaHora.split(delimiter);
        fecha=xv[0];
        hora=xv[1];
        String delimiter2=":";
        String[]xv2=fechaHora.split(delimiter2);
        horas=xv2[0];
        minutos=xv2[1];
        segundos=xv2[2];
       
        this.root.getChildren().add(new Archivo().obtenerImagen(Constante.FONDO,Constante.ALTO,Constante.ANCHO));
       
        this.nivel = 1;
        this.piratip = 1;
        cargarNivel(nivel,nombre,score,vida);
        Scene scene = new Scene(root);
        scene.setOnKeyPressed(new moveKeyHandle());
        this.stage.setScene(scene);
        stage.setResizable(false);
    }
           public void cargarNivel(int i , String nombre,int score,int vidas)
    {
    
        configInicial();
        level = new Nivel(i,root,nombre,piratip);//en vez de jugar puse root
        level.getJugador().setNombre(nombre);
        level.getJugador().setScore(score);
        level.getJugador().setVidas(vidas);
        panelMenu();
        level.start();
    }
               public void configInicial()
    {
        jugar = new Pane();        
        jugar.setMinSize(Constante.ANCHO, Constante.ALTO);
        
        root.setCenter(jugar); 
        root.getCenter();
       
        
    } 

       public void panelMenu()
    {
        Group grupo = new Group();
        Label nombre = new Label(" "+level.getJugador().getNombre());
        nombre.setLayoutX(30);
        nombre.setLayoutY(130);
        puntaje = new Label(" "+level.getJugador().getScore());
        puntaje.setLayoutX(30);
        puntaje.setLayoutY(175);
        vidas = new Label(" "+level.getJugador().getVidas());
        vidas.setLayoutX(30);
        vidas.setLayoutY(220);
        balas = new Label(" "+ level.getJugador().getPirata().getBalas());
        balas.setLayoutX(30);
        balas.setLayoutY(265);
        bombas = new Label (" "+ level.getJugador().getPirata().getBomba());
        bombas.setLayoutX(30);
        bombas.setLayoutY(305);
        grupo.getChildren().addAll(new Archivo().obtenerImagen(Constante.FONDO_MENU,Constante.ALTO,200),nombre,puntaje,vidas,balas,bombas);
        grupo.setAutoSizeChildren(true);
        grupo.autosize();
        Rectangle sal = new Rectangle(80, 40,new ImagePattern(new Image("file:src\\imagenes\\bEscapar.png")));
        sal.setLayoutX(560);
        sal.setLayoutY(0);
        sal.setOnMouseReleased(bsalir);
        Rectangle pausa = new Rectangle(80, 40,new ImagePattern(new Image("file:src\\imagenes\\bPausa.png")));
        pausa.setLayoutX(560);
        pausa.setLayoutY(50);
        pausa.setOnMousePressed(bpausa);
        Circle circle = new Circle(cx, cy,25.0f,new ImagePattern(new Image("file:src\\imagenes\\bomb.png")));
        circle.setCursor(Cursor.HAND);
        circle.setOnMousePressed(circleOnMousePressedEventHandler);
        circle.setOnMouseDragged(circleOnMouseDraggedEventHandler);
        circle.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override public void handle(MouseEvent event) {
                if (level.getJugador().getPirata().getBomba() == 0)
                    {                        
                        bombas.setText("No tiene Bombas ");
                    }
                    
                    else if (level.getJugador().getPirata().dispararBomb((Pane) root.getCenter(),level.getPiratasE(),nX,nY))
                    {
                         // level.getJugador().getPirata().dispararNBomb((Pane) root.getCenter(),level.getPiratasE(),nX,nY);
                        
                        Sonido.disparoBomba();
                        level.getJugador().setScore(level.getJugador().getScore()+20);
                        puntaje.setText(" "+level.getJugador().getScore());
                        level.getJugador().getPirata().setBomba(level.getJugador().getPirata().getBomba()-1);
                        level.setPiratasVivos(level.getPiratasVivos()-1);
                        bombas.setText(" "+ level.getJugador().getPirata().getBomba());
                        
                    }
                    else
                    {
                        Sonido.disparoBomba();
                        level.getJugador().getPirata().setBomba(level.getJugador().getPirata().getBomba()-1);
                        bombas.setText(" "+ level.getJugador().getPirata().getBomba());
                    }
                event.consume();
            }
        }); 
        
        root.getChildren().addAll(circle,sal,pausa);
       
         
       
        root.setRight(grupo);
    }
  private class moveKeyHandle implements EventHandler<KeyEvent>
    {      
      
        @Override
    
        public void handle(KeyEvent event) 
        {   
            
           
            boolean disparo = false;
            if (!pause)
            {  
                tmp= nivel;
                if (event.getCode() == KeyCode.UP)
                    level.getJugador().getPirata().moverY(-1*Constante.RECORRIDO_PIRATA);
                if (event.getCode() == KeyCode.DOWN)
                    level.getJugador().getPirata().moverY(Constante.RECORRIDO_PIRATA);
                //Dispara con la tecla espaciadora
                if (event.getCode() == KeyCode.SPACE)
                {                    
                    Sonido.disparoBala(); 
                    if (level.getJugador().getPirata().disparar((Pane) root.getCenter(),level.getPiratasE()))
                    {
                        disparo = true;
                        level.getJugador().setScore(level.getJugador().getScore()+20);
                        puntaje.setText(" "+level.getJugador().getScore());
                        level.getJugador().getPirata().setBalas(level.getJugador().getPirata().getBalas()+1);
                        balas.setText(" "+ level.getJugador().getPirata().getBalas());
                        level.setPiratasVivos(level.getPiratasVivos()-1);
                        
                    }
                    else
                    {
                        level.getJugador().getPirata().setBalas(level.getJugador().getPirata().getBalas()+1);
                        balas.setText(" "+ level.getJugador().getPirata().getBalas());
                    }
                }
                //Dispara solo moviendo el pirata principal
                if (level.getJugador().getPirata().ConfirmarLimites((Pane) root.getCenter(), level.getPiratasE()))
                {                    
                    Sonido.disparoBala(); 
                    if (level.getJugador().getPirata().disparar((Pane) root.getCenter(),level.getPiratasE()))
                    {
                        disparo = true;
                        level.getJugador().setScore(level.getJugador().getScore()+20);
                        puntaje.setText(" "+level.getJugador().getScore());
                        level.getJugador().getPirata().setBalas(level.getJugador().getPirata().getBalas()+1);
                        balas.setText(" "+ level.getJugador().getPirata().getBalas());
                        level.setPiratasVivos(level.getPiratasVivos()-1);
                        
                    }
                    else
                    {
                        level.getJugador().getPirata().setBalas(level.getJugador().getPirata().getBalas()+1);
                        balas.setText(" "+ level.getJugador().getPirata().getBalas());
                    }
                }
            
                if (event.getCode() == KeyCode.P)
                {
//                    
                    velocidad = (long) level.getVelocidad();
                    level.setVelocidad(500);
                    gamePause();            
                    pause = true;
                } 
                if (event.getCode() == KeyCode.ESCAPE)
                {                    
                    level.setPiratasVivos(0);
                    nivel = 5;
                    //level.interrupt();
                    //level.getJugador().setGameover(true);
                } 
              
                            }
           
            
            else if (event.getCode() == KeyCode.P)
            {
               root.setCenter(jugar);
               level.setVelocidad(velocidad);
               velocidad = 0;
//               if (level.isInterrupted())
//                    level.notifyAll();
               pause = false;
            }            
            if (level.getJugador().isGameover())
            {
                
                nivel = 5;
                level.setPiratasVivos(0);
               
            }
       
            if (level.getPiratasVivos() == 0  )
            {
                if (nivel <3)
                {
                    level.setPiratasVivos( 0);
                    nivelCompletado();
                    cx=230;
                    cy=35;
                    level.getJugador().getPirata().setBomba(1);
                    
                }
                else if (nivel == 5)
                    //level.setCompletado(true);
                {
                    try {
                        gameOver(Constante.FONDO_OVER);
                    } catch (IOException ex) {
                        Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                else if(nivel >= 3)
                {
                    try {
                        
                        gameOver(Constante.FONDO_WIN);
                    } catch (IOException ex) {
                        Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            //si quisiera q se sumaran las vidas por matar en racha a los piratas
            /*if (level.getJugador().getScore()%10000 == 0 && level.getJugador().getScore()>0 && disparo)
            {
                level.getJugador().setVidas(level.getJugador().getVidas()+1);
                //Sonido.ganaVida();
                vidas.setText("Vidas : "+ level.getJugador().getVidas()+"");
            }*/
          
        }        
    }
      public void nivelCompletado()
    {
        root.getChildren().add(new Archivo().obtenerImagen(Constante.FONDO,Constante.ALTO,Constante.ANCHO));
        stage.hide();
        levelCompleto = new Stage(StageStyle.TRANSPARENT);
        Group grupo = new Group();
        Scene scene = new Scene(grupo,600,600);
        levelCompleto.setScene(scene);
        levelCompleto.show();
             Button jck= new Button();
        jck.setGraphic(new Archivo().obtenerImagen(Constante.BOT_JACK, 300, 150));
        jck.setStyle("-fx-background-color: transparent;");
        jck.setCursor(Cursor.HAND);
        jck.setLayoutX(50);
        jck.setLayoutY(180);
         Button bb= new Button();
         jck.setCursor(Cursor.HAND);
        bb.setGraphic(new Archivo().obtenerImagen(Constante.BOT_BARBOSA, 300, 150));
        bb.setStyle("-fx-background-color: transparent;");
        bb.setLayoutX(215);
        bb.setLayoutY(180);
         Button dj= new Button();
        dj.setGraphic(new Archivo().obtenerImagen(Constante.BOT_DAVID_JONES, 300, 150));
        dj.setStyle("-fx-background-color: transparent;");
        dj.setLayoutX(380);
        dj.setLayoutY(180);
        Button at= new Button("Regresar");
        at.setLayoutX(220);
        at.setLayoutY(510); 
        grupo.getChildren().addAll(new Archivo().obtenerImagen(Constante.ELIGE_CAP,600,600),jck,bb,dj,at);
                       jck.setOnAction(e ->{
                    
                 levelCompleto.close();
                nivel +=1;
                piratip = 1;
                cargarNivel(nivel,level.getJugador().getNombre(),level.getJugador().getScore(),level.getJugador().getVidas());
                stage.show();
       
        });
            bb.setOnAction(e ->{
                    
                 levelCompleto.close();
                nivel +=1;
                piratip = 2;
                cargarNivel(nivel,level.getJugador().getNombre(),level.getJugador().getScore(),level.getJugador().getVidas());
                stage.show();
       
        });
         
          dj.setOnAction(e ->{
              if(nivel==2){
                levelCompleto.close();
                 nivel +=1;
                piratip = 3;
                cargarNivel(nivel,level.getJugador().getNombre(),level.getJugador().getScore(),level.getJugador().getVidas());
                stage.show();
       }
              else{
                   Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(null);
            alert.setHeaderText(null);
            alert.setContentText("Tienes que llegar al nivel 3 para poder jugar con este pirata");
            alert.showAndWait(); 
               
                  }
        });
          at.setOnAction(new overAction(0));
         
    }
      public void gameOver(String ruta) throws IOException
    {               
        System.out.println("Entra a Game over");
        levelCompleto = new Stage(StageStyle.TRANSPARENT);
        stage.hide();                
        Group grupo = new Group();
        Scene scene = new Scene(grupo,500,500);
        levelCompleto.setScene(scene);
        levelCompleto.show();
        levelCompleto.centerOnScreen();
        //stage.setScene(scene);
        grupo.getChildren().add(new Archivo().obtenerImagen(ruta, 500, 500));
        root.setCenter(grupo);
        Button b1 = new Button("Regresar al Menu Principal");        
        b1.setLayoutX(250);
        b1.setLayoutY(250);
     
        b1.setOnAction(new overAction(0));
        grupo.getChildren().addAll(b1);
        Archivo a = new Archivo();
        LinkedList<Archivo> archivos=a.cargarArchivos();
        String gano;
           Timeline timeline = new Timeline();
	 Text currTimeText = new Text("Current time: 0 secs" );
	 currTimeText.setBoundsType(TextBoundsType.VISUAL);
          Duration time = new Duration(Double.POSITIVE_INFINITY);
          timeline.setCycleCount(0);
          timeline.play();
         
	 timeline.currentTimeProperty().addListener(new InvalidationListener() {
			public void invalidated(Observable ov) {
				
					int time = (int) timeline.getCurrentTime().toSeconds();
					currTimeText.setText("el tiempo transcurrido:v " + time + " segundos");
				}
			});
        if(nivel >= 3&& tmp==3)
            gano="S";
        
         gano="N";
       
         a.IngresarNuevosDatos(archivos, level.getJugador().getNombre(),hora,Integer.toString(level.getJugador().getScore()), fecha,Integer.toString(tmp), gano);
        
        System.out.println("sale a Game over");
    } 
          public void gamePause()
    {
        Rectangle dpausa = new Rectangle(80, 40,new ImagePattern(new Image("file:src\\imagenes\\bContinuar.png")));
        dpausa.setLayoutX(560);
        dpausa.setLayoutY(50);
        dpausa.setOnMousePressed(bdpausa);
        Group grupo = new Group();
        Label lab = new Label ("El Juego fue Pausado.");
        lab.setTextFill(Color.WHITE);
        lab.setLayoutX(Constante.ANCHO/2);
        lab.setLayoutY(Constante.ALTO/2);
        grupo.getChildren().addAll(new Archivo().obtenerImagen(Constante.FONDO, Constante.ALTO, Constante.ANCHO),lab,dpausa);
        root.setCenter(grupo);
    }
  
     private class overAction implements EventHandler<ActionEvent>
    {
        private int opcion;
        private overAction(int op) 
        {           
           this.opcion = op;
        }

        @Override
        public void handle(ActionEvent event) 
        {
            if (this.opcion == 0)//esto lo puse para salir
            {
                stage.close();
                levelCompleto.close();
                try {
                    MenuPrincipal mp= new MenuPrincipal(stage);
                } catch (IOException ex) {
                    Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                
            }
            else//esto es para reiniciar el nivel pero no se usa
            {
                levelCompleto.close();
                nivel =1;
                cargarNivel(nivel,level.getJugador().getNombre(),0,3);
                stage.show();
            }
        }        
    }
     private class move2KeyHandle implements EventHandler<KeyEvent>
    {        
        @Override
        public void handle(KeyEvent event) 
        {    
                levelCompleto.close();
                nivel +=1;
                //root.getChildren().add(new Archivo().obtenerImagen(Constante._fondo,Constante._alto,Constante._ancho));
                cargarNivel(nivel,level.getJugador().getNombre(),level.getJugador().getScore(),level.getJugador().getVidas());
                stage.show();
            
        }
    }
 EventHandler<MouseEvent> circleOnMousePressedEventHandler = 
        new EventHandler<MouseEvent>() {
 
        @Override
        public void handle(MouseEvent t) {
            orgSceneX = t.getSceneX();
            orgSceneY = t.getSceneY();
            orgTranslateX = ((Circle)(t.getSource())).getTranslateX();
            orgTranslateY = ((Circle)(t.getSource())).getTranslateY();
        }
    };
     
    EventHandler<MouseEvent> circleOnMouseDraggedEventHandler = 
        new EventHandler<MouseEvent>() {
 
        @Override
        public void handle(MouseEvent t) {
            double offsetX = t.getSceneX() - orgSceneX;
            double offsetY = t.getSceneY() - orgSceneY;
            nX = orgTranslateX + offsetX;
            nY= orgTranslateY + offsetY;
             
            ((Circle)(t.getSource())).setTranslateX(nX);
            ((Circle)(t.getSource())).setTranslateY(nY);
           if( level.getJugador().getPirata().getBomba()==0){
              ((Circle)(t.getSource())).setTranslateX(2000);
            ((Circle)(t.getSource())).setTranslateY(2000); 
           }
               
        }
    };
     EventHandler<MouseEvent> bsalir = 
        (MouseEvent t) -> {
            try {
                gameOver(Constante.FONDO_OVER);
            } catch (IOException ex) {
                Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
            }
    };
       EventHandler<MouseEvent> bpausa = 
        new EventHandler<MouseEvent>() {
 
        @Override
        public void handle(MouseEvent t) {
             velocidad = (long) level.getVelocidad();
                    level.setVelocidad(500);
                    gamePause();            
                    pause = true;
              
        }
    };
           EventHandler<MouseEvent> bdpausa = 
        new EventHandler<MouseEvent>() {
 
        @Override
        public void handle(MouseEvent t) {
           root.setCenter(jugar);
               level.setVelocidad(velocidad);
               velocidad = 0;
//               if (level.isInterrupted())
//                    level.notifyAll();
               pause = false;
              
        }
    };
           
}
