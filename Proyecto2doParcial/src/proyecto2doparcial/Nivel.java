/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2doparcial;

import java.util.LinkedList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.layout.Pane;

/**
 *
 * @author Ivan Cedeno
 * @author Steffy Freire
 * @author Kleber Avelino
 * Esta clase fue creada para poder controlar los diferentes piratas
 * con threads.
 * En esta clase se controlan desde la salida hasta los movimientos de los piratas.
 */
public class Nivel extends Thread 
{
    static final int LV1CAN = 15;
    static final double LV1VEL = 40;
    static final int LV2CAN = 25;
    static final double LV2VEL = 30;
    static final int LV3CAN = 35;
    static final double LV3VEL = 20;
    
    private int cantidad;
    private double velocidad;
    private int level;
    private LinkedList<PiratasEnemigo> piratasE;
    private boolean completado;
    private Jugador jugador;
    private int PiratasVivos;
    private Pane panel;

    public Nivel(int level,Pane panel, String nombre,int i) 
    {        
        panel = panel;
        jugador = new Jugador(nombre,panel,i);
        this.level= level;
        switch (level)
        {
            case 1:
            {
                this.cantidad = LV1CAN;
                this.velocidad = LV1VEL;break;
            }
            case 2:
            {
                this.cantidad = LV2CAN;
                this.velocidad = LV2VEL;;break;
            }
            case 3:
            {
                this.cantidad = LV3CAN;
                this.velocidad = LV3VEL;break;
                
            }
        }
        piratasE = new LinkedList<>();
        this.completado=false;
        cargarPiratasE(panel);
        this.PiratasVivos = piratasE.size();
    }

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    
    public int getPiratasVivos() {
        return PiratasVivos;
    }

    public void setPiratasVivos(int PiratasVivos) {
        this.PiratasVivos = PiratasVivos;
    }

    public boolean isCompletado() 
    {         
        return completado;
    }

    public void setCompletado(boolean completado) {
        this.completado = completado;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
    
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(double velocidad) {
        this.velocidad = velocidad;
    }

    public LinkedList<PiratasEnemigo> getPiratasE() {
        return piratasE;
    }

    public void setPiratasE(LinkedList<PiratasEnemigo> piratasE) {
        this.piratasE = piratasE;
    }
    
    public void cargarPiratasE(Pane panel)            
    {
        System.out.println(" cantida de PiratasE "+this.cantidad);
        
        int col= 25;
                 for (int i =0;i < this.cantidad ;i++)
            {
                PiratasEnemigo pe1 = new PiratasEnemigo1(600+col,260+(new Random().nextInt(5))*40 ,panel);
                piratasE.add(pe1);
              col= col+25;
             }
    }
    
    public void run()
    {
        while(this.PiratasVivos > 0)
        {
            try {
               // System.out.println(" cantida de piratasEnemigos "+ piratasE.size()); CON ESTO VEO CUANTOS PIRATAS VIVOS
           
                
                piratasE.forEach((pe) -> {
                    //System.out.println(" cantida de piratasEnemigos "+ pe.getCentroX()); //CON ESTO VEO LAS POSICIONES
                    if (!pe.isVisible())
                        this.PiratasVivos -= 1;
                    else
                    {
                        if ((pe.getCentroX()) <= 0)
                        {                                                
                            pe.destruirpirataE();
                            this.PiratasVivos -= 1;
                            this.jugador.setVidas(this.jugador.getVidas()-1);
                            if (this.jugador.getVidas() == 0)
                            {
                                this.PiratasVivos = 0;
                                this.jugador.setGameover(true);                           
                            }
                        }
                        else
                        {
                            pe.setCentroX(pe.getCentroX()-1);          
                            pe.setupPirata();                        
                        }
                    }                    
                });
                Thread.sleep((long) this.velocidad);
            } catch (InterruptedException ex) {
                 this.jugador.setGameover(true);
                Logger.getLogger(Nivel.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        System.out.println("sale del while nivel");
        
    }

    
}
