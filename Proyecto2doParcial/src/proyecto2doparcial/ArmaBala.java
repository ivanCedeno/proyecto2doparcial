/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2doparcial;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.util.Duration;

/**
 *
 * @author Ivan Cedeno
 *  @author Steffy Freire
 * @author Kleber Avelino
 * ArmaBala es el objeto que se asigna al pirata principal
 */
public class ArmaBala extends Arma 
{
    private Ellipse bala;
    private Pane panel;

    public Ellipse getBala() {
        return bala;
    }

    public void setBala(Ellipse bala) {
        this.bala = bala;
    }
/*
    el constructor recibe las coordenadas de centro en X y Y ademas del Pane en donde sera ubicado el objeto
    */
    public ArmaBala(double centroX, double centroY,Pane panel) {
        super(Constante.ALTO_BALA_PIRATA, Constante.ANCHO_BALA_PIRATA, centroX, centroY);
        bala = new Ellipse();
        setupBala();
        panel = panel;
        panel.getChildren().add(bala);
    }
    /*
    el metodo permite inicializar las características del arma
    */
    public void setupBala()
    {
        this.bala.setVisible(this.isVisible());
        this.bala.setFill(Color.YELLOW);
        this.bala.setRadiusX(this.getAncho()/2);
        this.bala.setRadiusY(this.getAlto()/2);
        this.bala.setCenterX(this.getCentroX());
        this.bala.setCenterY(this.getCentroY());
    }
/*
    se sobreescribe el metodo mover de la clase Arma
    */
    @Override
    public double mover(int i) 
    { 
        
        KeyValue xCoord = new KeyValue (bala.centerXProperty(), bala.getCenterX()+i);
        KeyFrame f = new KeyFrame (Duration.seconds(0.7), xCoord);
        Timeline animation = new Timeline(f);
        animation.play();
        this.setCentroX(bala.getCenterX()+i);     
        return this.getCentroX()-1;        
        
    }    
/*
    se sobreescribe el metodo destruir de la clase Arma utilizando una animación
    */
    @Override
    public void destruir() {
        if (!this.isVisible())
        {
            for (int x = 0; x< this.bala.getRadiusX()+10;x++)
                {
                    KeyValue radioy = new KeyValue (this.bala.radiusYProperty(), this.bala.getRadiusY()-x);
                    KeyValue radiox = new KeyValue (this.bala.radiusXProperty(), this.bala.getRadiusX()-x);
                    KeyFrame f1 = new KeyFrame (Duration.seconds(3.5), radioy);                                     
                    KeyFrame f2 = new KeyFrame (Duration.seconds(3.5), radiox);                                     
                    Timeline anrioY = new Timeline(f1);
                    Timeline anrioX = new Timeline(f2);
                    anrioY.play();
                    anrioX.play();
                }
        }
    }

    



}
