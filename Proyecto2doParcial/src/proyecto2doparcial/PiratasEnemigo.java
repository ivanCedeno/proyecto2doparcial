/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2doparcial;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.*;
import javafx.util.Duration;


/**
 *
 * @author Ivan Cedeno
 * @author Steffy Freire
 * @author Kleber Avelino
 */
public abstract class PiratasEnemigo extends Piratas
{
    private String tipo;
    private int desBalas;
    private int desBomb;    
    private Ellipse base;
    private Pane panel;

    public Ellipse getBase() {
        return base;
    }

    public void setBase(Ellipse base) {
        this.base = base;
    }
    /*
    Constructor recibe coordenadas de X y Y , Panel y el tipo(pirata gris)
    */
    public PiratasEnemigo(double centroX, double centroY,Pane panel,String tipo) 
    {        
        super(Constante.ALTO_PIRATA_E, Constante.ANCHO_PIRATA_E, centroX, centroY);
        base = new Ellipse();
        
        this.tipo = tipo;
       switch (tipo)
        {
            case"gray":this.desBalas= 1;break;
            case"green":this.desBalas= 3;break;
            case"red":this.desBalas= 5;break;
        }  
        this.desBomb =1;        
        this.setupPirata();
      
        Image imB= new Image("file:src\\imagenes\\Buggy.png");
        base.setFill(new ImagePattern(imB));
    
        panel.getChildren().addAll(base);
       centroX=panel.getLayoutX();
        panel = panel;
    }
    /*
    retorna la cantidad de balas con la que el objeto sera destruido
    */
    public int getDesBalas() 
    {
        return desBalas;
    }
/*
    permite dar valor a la cantidad de balas con las que es destruido el ovni
    */
    public void setDesBalas(int desBalas) 
    {
        this.desBalas = desBalas;
    }
/*
    retorna la cantidad de misiles con los que el objeto es destruido
    */
    public int getDesBomb() 
    {
        return desBomb;
    }
    /*
    permite dar valor a la cantidad de misiles
    */

    public void setDesBomb(int desBomb) {
        this.desBomb = desBomb;
    }
/*  
    el objeto es destruido cuando sus propiedades de balas y misiles lllega a 0
    */
   
    public boolean destruir()
    {        
        if (this.desBalas == 0 || this.desBomb == 0)
        {
            for (int x = 0; x< this.getBase().getRadiusX()+4;x++)
                {
                    KeyValue radioy = new KeyValue (this.getBase().radiusYProperty(), this.getBase().getRadiusY()-x);
                    KeyValue radiox = new KeyValue (this.getBase().radiusXProperty(), this.getBase().getRadiusX()-x);
                  
                    
                    KeyFrame f1 = new KeyFrame (Duration.seconds(2.5), radioy);                                     
                    KeyFrame f2 = new KeyFrame (Duration.seconds(2.5), radiox);                                     
                                                    
                    
                    
                    Timeline anrioY = new Timeline(f1);
                    Timeline anrioX = new Timeline(f2);
                    
                 
                    anrioY.play();
                    anrioX.play();
                   
                }
            return true;
            
        }
        return false;
    }
    /*
    metodo se produce cuando la nave llega a la parte inferior de la ventana
    */
    public void destruirpirataE()
    {
        for (int x = 0; x< this.getBase().getRadiusX()+4;x++)
        {
                    KeyValue radioy = new KeyValue (this.getBase().radiusYProperty(), this.getBase().getRadiusY()-x);
                    KeyValue radiox = new KeyValue (this.getBase().radiusXProperty(), this.getBase().getRadiusX()-x);
                 
                    
                    KeyFrame f1 = new KeyFrame (Duration.seconds(1), radioy);                                     
                    KeyFrame f2 = new KeyFrame (Duration.seconds(1), radiox);                                     
                                                         
                    
                    
                    Timeline anrioY = new Timeline(f1);
                    Timeline anrioX = new Timeline(f2);
                    
                    
                    
                    anrioY.play();
                    anrioX.play();
                    
        }
    }

    public void moverX(double cantidad) 
    {
        
        
    }
    
    @Override
    public void moverY(double cantidad) {
        
    }
/*
    permite inicializar las propiedades del objeto
    */    

    @Override
    public void setupPirata() 
    {
        this.base.setVisible(this.isVisible());
        this.base.setRadiusX(this.getAlto());
        this.base.setRadiusY(this.getAncho()-10);
        this.base.setCenterX(this.getCentroX());
        this.base.setCenterY(this.getCentroY());
        this.base.setStrokeWidth(2);
        
 
    }

    /*
    permite al objeto dispara , recibe un panel en el cual se ubicara el objeto Arma y retorna el valor si impacto a otro objeto
    */
    
    public boolean disparar(Pane panel,PirataPrincipal pp) 
    {
        ArmaEnemiga ao = new ArmaEnemiga(this.getCentroX(),this.getCentroY(), panel,pp);
        Thread th = new Thread(ao);
        th.start();
        return true;
    }

    @Override
    public boolean isMover(double cantidad) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
}
