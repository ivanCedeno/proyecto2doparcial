/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2doparcial;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.STYLESHEET_MODENA;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Ivan Cedeño
 * @author Steffy Freire
 * @author Kleber Avelino
 * En esta clase creamos todos los layouts que usaremos
 * en el menu de inicio del juego.
 */
public class MenuPrincipal {
    private TextField nombre;
    private Stage ventana,gm;
    private Scene sc1,sc2,sc3,sc4,sc5,game;
    public Stage salir;
    private LinkedList<Archivo> arch;
    public MenuPrincipal(Stage sg) throws IOException{
        this.salir= sg;
        //layout Del Menu Principal
        Sonido.sonidoFondo();
        
    salir = new Stage(StageStyle.TRANSPARENT);
        ventana=salir;
        Group grupo = new Group();
        sc1=new Scene(grupo,600,600,Color.TRANSPARENT);
        ventana.setTitle(STYLESHEET_MODENA);
        ventana.setScene(sc1);
        ventana.centerOnScreen();
         salir.show();
         Button b1 = new Button();
        b1.setGraphic(new Archivo().obtenerImagen(Constante.BOTON_JUEGO, 20, 80));
        b1.setStyle("-fx-background-color: transparent;");
        b1.setLayoutX(240);
        b1.setLayoutY(180);
          Button b2 = new Button();
        b2.setGraphic(new Archivo().obtenerImagen(Constante.BOTON_HISTORIAL, 30, 140));
        b2.setStyle("-fx-background-color: transparent;");
        b2.setLayoutX(210);
        b2.setLayoutY(230);
          Button b3 = new Button();
        b3.setGraphic(new Archivo().obtenerImagen(Constante.BOTON_INSTRUCCIONES, 30, 100));
        b3.setStyle("-fx-background-color: transparent;");
        b3.setLayoutX(230);
        b3.setLayoutY(280);
          Button b4 = new Button();
        b4.setGraphic(new Archivo().obtenerImagen(Constante.BOTON_CREDITOS, 20, 70));
        b4.setStyle("-fx-background-color: transparent;");
        b4.setLayoutX(240);
        b4.setLayoutY(330);
          Button b5 = new Button();
        b5.setGraphic(new Archivo().obtenerImagen(Constante.BOTON_SALIR,20, 60));
        b5.setStyle("-fx-background-color: transparent;");
        b5.setLayoutX(240);
        b5.setLayoutY(380);
        grupo.getChildren().addAll(new Archivo().obtenerImagen(Constante.FONDO_INICIO,600,600),b1,b2,b3,b4,b5);
        b1.setOnAction(e ->ventana.setScene(sc2));
        b5.setOnAction(e ->ventana.close() );
              //layout de seleccion de pirata
         
        Group gp2 = new Group();
        sc2=new Scene(gp2,600,600,Color.TRANSPARENT);
        Button jck= new Button();
        jck.setGraphic(new Archivo().obtenerImagen(Constante.BOT_JACK, 300, 150));
        jck.setStyle("-fx-background-color: transparent;");
        jck.setCursor(Cursor.HAND);
        jck.setLayoutX(50);
        jck.setLayoutY(180);
         Button bb= new Button();
        bb.setGraphic(new Archivo().obtenerImagen(Constante.BOT_BARBOSA, 300, 150));
        bb.setStyle("-fx-background-color: transparent;");
        bb.setLayoutX(215);
        bb.setLayoutY(180);
         Button dj= new Button();
        dj.setGraphic(new Archivo().obtenerImagen(Constante.BOT_DAVID_JONES, 300, 150));
        dj.setStyle("-fx-background-color: transparent;");
        dj.setLayoutX(380);
        dj.setLayoutY(180);
        nombre = new TextField("Ingrese tu nombre aqui:");
        nombre.setLayoutX(60);
        nombre.setLayoutY(150);        
        nombre.setAlignment(Pos.CENTER);
        nombre.selectAll();
        Button at= new Button("Regresar");
        at.setLayoutX(220);
        at.setLayoutY(510);
        gp2.getChildren().addAll(new Archivo().obtenerImagen(Constante.ELIGE_CAP,600,600),jck,bb,dj,nombre,at);
        at.setOnAction(e ->ventana.setScene(sc1));
        jck.setOnAction(e ->{ventana.setScene(game);
        ventana.setScene(sc1);
        ventana.hide();
        Stage gm = new Stage(StageStyle.TRANSPARENT);
               gm.centerOnScreen();
                Juego tablero = new Juego(nombre.getText(),0,gm,salir);
                gm.show();
                
        });
        bb.setOnAction(e ->{
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle(null);
            alert.setHeaderText(null);
            alert.setContentText("Tienes que llegar al nivel 2 para poder jugar con este pirata");
            alert.showAndWait();
        });
              dj.setOnAction(e ->{
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle(null);
            alert.setHeaderText(null);
            alert.setContentText("Tienes que llegar al nivel 3 para poder jugar con este pirata");
            alert.showAndWait();             
        });
              //layout de historial de jugadores
        
         b2.setOnAction(e ->ventana.setScene(sc3));
       
         
          HBox hb = new HBox();
          Text t= new Text();
          t.setX(0);
          t.setY(0);   
           Archivo a = new Archivo();
          a.cargarArchivos();
           
            ObservableList<String> pp= FXCollections.observableArrayList();
            for(int i=0;i<a.cargarArchivos().size();i++){
                System.out.println(a.cargarArchivos().size());
                 pp.add(a.cargarArchivos().get(i).toString());
            }
                
               
            
            String tmp;
    
            ListView<String> listView = new ListView<String>(pp);
           
            listView.setStyle("-fx-background-insets: 0 ;");
           // listView.resizeRelocate(1, 1,300, 50);
            listView.setPrefSize(300, 300);
            listView.setLayoutX(100);
             listView.setLayoutY(200);
             Label histor= new Label("Historial De Jugadores");
            histor.setLayoutX(80);
            histor.setLayoutY(160);
             
              Group gp3 = new Group();
         Button at2= new Button("Regresar");
        at2.setLayoutX(220);
        at2.setLayoutY(510);
        at2.setOnAction(e ->ventana.setScene(sc1));
        sc3=new Scene(gp3,600,600,Color.TRANSPARENT);
        gp3.getChildren().addAll(new Archivo().obtenerImagen(Constante.MENU_INSTRUCCIONES,600,600),at2, listView,histor);
            
 
        
        
         
        
        //layout de las instrucciones del juego
        b3.setOnAction(e ->ventana.setScene(sc4));
        Button at3= new Button("Regresar");
         Text intruc = new Text("El juego consiste en detener a los piratas enemigos para que no tomen el tesoro \n"
         +"resguardado por el capitán del barco.\n"
         +"Además el juego estará compuesto por tres niveles: Básico, Intermedio y Avanzado.Cada vez\n"
         +"que se logré derribar a todos los piratas se podrá acceder al siguiente nivel.\n"
         +"*Para el nivel básico, deben aparecer un total de 15 piratas a velocidad normal de\n "
                 + "deaparición y de avance.\n"
         +"*Para el nivel Intermedio, deben aparecer un total de 25 piratas con mayor velocidad de\n"
                 + " aparición y de avance.\n"
         +"*Para el nivel Avanzado, deben aparecer un total de 35 piratas con mucha más velocidad de\n "
                 + " aparición y de avance.\n"
         +"\n\t\t\t MOVIMIENTOS  \n"
         +"Podras moverte con el teclado solo hacia arriba o abajo.\n"
         +"Cada vez que logres pasar un nivel,tendras la opción de elegir otro capitán como pirata.\n"
         +"Disparas con la barra espaciadora, pero tambien tendras la opcion de disparo automatico.\n"
         +"Cuando quieras pausar el juego presionala tecla **P** y de igual manera para despausar.\n"
         +"Cuando quieras Salir el juego presionala tecla **Esc** .\n"
         +"Tendras una bomba que podras moverla con el mouse con un mayor rango de alcance\n"
         + "para destruir piratas enemigos ¡no la despedicies!.\n");
      
        intruc.setLayoutX(50);
        intruc.setLayoutY(200);
        at3.setLayoutX(220);
        at3.setLayoutY(510);
        at3.setOnAction(e ->ventana.setScene(sc1));
        Group gp4 = new Group();
        sc4=new Scene(gp4,600,600,Color.TRANSPARENT);
        gp4.getChildren().addAll(new Archivo().obtenerImagen(Constante.MENU_INSTRUCCIONES,600,600),intruc,at3);
        //layout de los creditos
        b4.setOnAction(e ->ventana.setScene(sc5));
        Group gp5 = new Group();
        Text cred = new Text("\t\t\t\t Proyecto del Segundo Parcial.\n"
                +"\t\t\t\t Programación Orientada a objetos.\n"
                +"\t\t\t\t Primer Término 2017-2018 \n"
                +"\t\t\t\t Resguardando el tesoro\n"
                +"Creadores:\n"
                +"*.-Ivan Lenin Cedeño Campoverde \n"
                +"*-Estefania Freire Tubay.\n" 
                +"*-Klever Avelino.");
        cred.setLayoutX(50);
        cred.setLayoutY(200);
         Button at4= new Button("Regresar");
        at4.setLayoutX(220);
        at4.setLayoutY(510);
        at4.setOnAction(e ->ventana.setScene(sc1));
        sc5=new Scene(gp5,600,600,Color.TRANSPARENT);
        gp5.getChildren().addAll(new Archivo().obtenerImagen(Constante.MENU_INSTRUCCIONES,600,600),at4,cred);
      }  
            
          
}
