/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2doparcial;
import java.util.LinkedList;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.*;

/**
 *
 * @author Ivan Cedeno
 * @author Steffy Freire
 * @author Kleber Avelino
 */
public class PirataPrincipal extends Piratas {
    private Ellipse base;
    private int balas;
    private int bombas;
    private int p;
    
 
    
 public PirataPrincipal(Pane panel,int i) {
        /*
        se envia los valores a la clase Naves
        */
        super(Constante.ALTO_PIRATA_P, Constante.ANCHO_PIRATA_P, Constante.PIRATA_X, Constante.ALTO-20);
        base = new Ellipse();
        Image imJ= new Image("file:src\\imagenes\\Pjack.png");
        Image imB= new Image("file:src\\imagenes\\pBarbosa.png");
        Image imD= new Image("file:src\\imagenes\\pdavid.png");
        if( i==  1 ){
            base.setFill(new ImagePattern(imJ));
            this.p = 1;
        }
        else if( i==  2 ){
            base.setFill(new ImagePattern(imB));
            this.p = 2;
        }
        else if( i==  3 ){
            base.setFill(new ImagePattern(imD));
            this.p = 3;
        }
        
     
 
        this.setupPirata();
        panel.getChildren().addAll(base);
    
        this.balas = 0;
        this.bombas= 1;
    }
    /*
    retorna el valor de la propiedad balas (ilimitado en este caso)
    */
    public int getBalas() {
        return balas;
    }

    public void setBalas(int balas) {
        this.balas = balas;
    }
/*
    retorna el valor de la propiedad misil (10 por cada nivel)
    */
    public int getBomba() {
        return bombas;
    }

    public void setBomba(int bomb) {
        this.bombas = bomb;
    }
    
    /*
    Mueve el objeto en la coordenada x en el inferior de la pantalla
    */
    @Override
    public void moverY(double cantidad) 
    {
        if(this.isMover(cantidad))
        {
            this.setCentroY(this.getCentroY()+cantidad);
            this.setupPirata();
           
            
        }
    }

    /*
    inicializa las propiedades del objeto
    */
    @Override
    public void setupPirata() 
    {
        this.base.setStrokeWidth(1);
        this.base.setRadiusX(this.getAlto());
        this.base.setRadiusY(this.getAncho());
        this.base.setCenterX(this.getCentroX()-160);
        this.base.setCenterY(this.getCentroY()-10);
        
       
        
     
        
    
    }
    /*
    recibe el panel sobre el que se ubicara el arma, ademas de la lista de piratasE, si se verifica el impacto
    retornara true caso contrario false
    */
    public boolean disparar(Pane panel, LinkedList<PiratasEnemigo> piratasE) 
    {
        ArmaBala am = new ArmaBala(this.getCentroX()-160,this.getCentroY()-10, panel);
        for (int i=1;i<= this.getCentroX()+440;i++)
        {            
            am.mover(i);            
            for (PiratasEnemigo pe : piratasE)
            {
                if(pe.getBase().intersects(am.getCentroX(), am.getCentroY(), am.getAlto(), am.getAncho())&& pe.isVisible() && am.isVisible())
                {   
                    am.setVisible(false);
                    am.destruir();
                    pe.setDesBalas(pe.getDesBalas()-1);                
                    if (pe.destruir())
                    {
                        pe.setVisible(false);
                        piratasE.remove(pe);
                        return true;
                    }                   
                    return false;
                }
            }            
        }
        return false;
    }
    //Cree esta funcion para poder saber cuando un pirata esta en el rango de disparo
         public boolean ConfirmarLimites(Pane panel, LinkedList<PiratasEnemigo> piratasE) 
    {    
            for (PiratasEnemigo pe : piratasE)
            {
                
                if(pe.getBase().getCenterX()<=490&&pe.getBase().getCenterY()==base.getCenterY())
                {   
        
                        return true;
                    }                   
                    return false;
                }
            
        return false;
    }
       
    /*
    recibe un panel sobre el cual se ubicará el objeto Bomba además una lista de piratasE
    si se verifica un impacto retorna true caso contrario retorna false
   */
    public boolean dispararBomb(Pane panel, LinkedList<PiratasEnemigo> piratE,double x,double y) 
    {
        ArmaBomba am = new ArmaBomba(x,y, panel);
                         
            for (PiratasEnemigo ov: piratE)
            {
                if(ov.getBase().intersects(am.getCentroX()+200, am.getCentroY(), am.getAlto(), am.getAncho())&& ov.isVisible() && am.isVisible())
                {                        
                    am.setVisible(false);
                    am.destruir();
                    ov.setDesBomb(ov.getDesBomb()-1);                
                    if (ov.destruir())
                    {
                       
                        ov.setVisible(false);
                        piratE.remove(ov);
                        return true;
                    }
                }
             
            }  
             
               
                
        
        return false;
    }
      public void dispararNBomb(Pane panel, LinkedList<PiratasEnemigo> piratE,double x,double y) 
    {
        ArmaBomba am = new ArmaBomba(x,y, panel);
                         
            for (PiratasEnemigo ov: piratE)
            {
                if(ov.getBase().intersects(am.getCentroX()+200, am.getCentroY(), am.getAlto(), am.getAncho()))
                {                        
                     
                    if (ov.destruir())
                    {
                       
                        ov.setVisible(false);
                        piratE.remove(ov);
                        
                    }
                }
             
                  am.setCentroX(am.getCentroX()+5);
                  am.setCentroY(am.getCentroY()-10);
            }  
                am.setVisible(false);
                    am.destruir();
                      
             
               
                
        
        
    }
     
/*
    verifica si el objeto puede moverse por las coordenadas Y   si se verifica que excede los limites
    de la venta retorna false caso contrario retorna true;
    */
    @Override
    public boolean isMover(double cantidad) 
    {
        if (cantidad < 0 )
            return (this.getCentroY()- base.getRadiusY()+cantidad-15 >= 200);
        return ((this.getCentroY()+base.getRadiusY()+ cantidad+15)<= Constante.ALTO);        
    }  
    
}
